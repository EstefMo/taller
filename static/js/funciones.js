function cargar(){
  sessionStorage.clear();
}
function sumar(id){
  numerito = $("#numero"+id).val();
  costo = $("#costo"+id).text();
  arreglo = JSON.parse(sessionStorage.getItem("datos"));
  var datos = [{
    "id": id,
    "costo": costo,
    "cantidad": numerito
}]
  if(arreglo != null){
    bandera = 0;
    for (var i = 0; i < arreglo.length; i++) {

      if(arreglo[i].id == id ){
        arreglo[i].cantidad = numerito;
        sessionStorage.setItem("datos", JSON.stringify(arreglo));
        bandera = 1;
      }
    }
    if (bandera == 0) {
          var data = {
            "id": id,
            "costo": costo,
            "cantidad": numerito
          };
          arreglo.push(data);
          sessionStorage.setItem("datos", JSON.stringify(arreglo));
      }
  }else{
    sessionStorage.setItem("datos", JSON.stringify(datos));
  }
  $(".comprar").css("display","block");
}
function realizarCompra(){
  $("#cen").css("display","flex");
  $(".modal").css("display","flex");
  arreglo = JSON.parse(sessionStorage.getItem("datos"));
  sumacostos = 0;
  sumarticulos = 0;
  for (var i = 0; i < arreglo.length; i++) {
      cant = arreglo[i].cantidad;
      costo = arreglo[i].costo;
      sumarticulos += parseInt(cant);
      sumacostos += (parseInt(cant) * parseInt(costo));
  }
  $("#articulos").html(sumarticulos);
  $("#total").html("$"+sumacostos)
}
function comprar(){
  arreglo = JSON.parse(sessionStorage.getItem("datos"));
  data = []
  for (var i = 0; i < arreglo.length; i++) {
    data.push([arreglo[i].id,parseInt(arreglo[i].cantidad)]);
  }
  data = JSON.stringify(data)
  $.ajax({
    url:'comprar',
    type:'GET',
    data: {data : data},
    success: function(data){
      if (data == "listo") {
        alert("compra realizada con exito")
        location.reload();

      }
    }
  });

}
